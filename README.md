# Handout Academy – Automatic Testing #

This handout should get the reader quickly up to speed with the tools as used in the TA Academy. After reading this handout, the reader should be able to configure a development street with an application under development with a Jenkins instance combined with both a Gitlab instance and SonarQube to have some insight in the software quality of the application. Basic functionality has been provided to build a Java application based on Maven.  

Without going into much detail, for the sake of clarity and ease of comprehension, we make use of micro-services offered by Docker. Docker can be seen as a form of packaging around applications to run them in an isolated way without having to install them. In addition to that, we also make use of Docker-Compose, an extension of Docker, to be able to run a set of Docker containers (applications) in a combined set.  

The requirements to run the development street as given are quite simple (keep in mind Linux is used in these examples, but Windows can be used as well with some adjustments); first one has to download and install both Docker and Docker Compose. Second, a secluded part of one’s hard drive has to be created that acts as persistent storage for the applications run by Docker. I chose the following location to store application data:

 * /data/jenkins/  
 * /data/gitlab/  
 * /data/gitlab/etc/  
 * /data/gitlab/var/log/  
 * /data/gitlab/var/opt/  
 * /data/sonar/  
 * /data/sonar/sonar_plugins_extensions/  
 * /data/sonar/sonar_plugins/  

Before starting everything, make sure these directories exist. Keep in mind a few subfolders have to be created as well, but we shall see these later. Lastly, we have to run the following docker-compose.yml file:


    version: '2'

    services:
      # Adminuser: root / password can be set up at first start
      gitlab:
        container_name: gitlab
        image: gitlab/gitlab-ce:9.2.4-ce.0
        ports:
          - "80:80"
          - "443:443"
          - "22:22"
        restart: always
        volumes:
          - "gitlab_etc:/etc/gitlab"
          - "gitlab_var_log:/var/log/gitlab"
          - "gitlab_var_opt:/var/opt/gitlab"

      # After initial start, one has to create a super user.
      jenkins:
        container_name: jenkins
        image: jenkins:2.46.3-alpine
        ports:
          - "8080:8080"
          - "50000:50000"
        restart: always
        volumes:
          - "jenkins:/var/jenkins_home"

      # Adminuser will be created after initial start
      portainer:
        container_name: portainer
        image: portainer/portainer:linux-amd64
        ports:
          - "9090:9000"
        volumes:
          - "/var/run/docker.sock:/var/run/docker.sock"

      # Adminuser: admin / admin
      sonar:
        container_name: sonarqube
        environment:
          - "SONARQUBE_JDBC_USERNAME=sonar"
          - "SONARQUBE_JDBC_PASSWORD=sonar"
          - "SONARQUBE_JDBC_URL=jdbc:postgresql://sonar_db:5432/sonar"
        image: sonarqube:6.4-alpine
        links:
        - "sonar_db:sonar_db"
        ports:
          - "9000:9000"
          - "9002:9002"
        restart: always
        volumes:
          - "sonar:/opt/sonarqube/conf"
          - "sonar:/opt/sonarqube/data"
          - "sonar:/opt/sonarqube/extensions"
        volumes_from:
          - "sonar_plugins"

      sonar_db:
        container_name: sonar_db
        image: postgres:9.6.3-alpine
        environment:
          - POSTGRES_USER=sonar
          - POSTGRES_PASSWORD=sonar
        ports:
          - "5432:5432"

      sonar_plugins:
        container_name: sonar_plugins
        image: sonarqube:6.4-alpine
        volumes:
          - "sonar_plugins_extensions:/opt/sonarqube/extensions"
          - "sonar_plugins_bundled:/opt/sonarqube/bundled-plugins"
        command: /bin/true

    volumes:
      gitlab_etc:
        driver_opts:
          type: none
          device: /data/gitlab/etc
          o: bind
      gitlab_var_log:
        driver_opts:
          type: none
          device: /data/gitlab/var/log
          o: bind
      gitlab_var_opt:
        driver_opts:
          type: none
          device: /data/gitlab/var/opt
          o: bind
      jenkins:
        driver_opts:
          type: none
          device: /data/jenkins
          o: bind
      sonar:
        driver_opts:
          type: none
          device: /data/sonar
          o: bind
      sonar_plugins_bundled:
        driver_opts:
          type: none
          device: /data/sonar_plugins
          o: bind
      sonar_plugins_extensions:
        driver_opts:
          type: none
          device: /data/sonar_plugins_extensions
          o: bind


As we see from the contents of the file above, it’s not a whole lot, right?

Save this file somewhere as docker-compose.yml. I have made a folder inside my homedir for this, named devstreet. (/home/user/devstreet).

In this directory, one can start all containers with the following command:

    docker-compose up –d

When the containers are started for the first time, it might take a while for the images to download. Luckily, this only has to happen once. When starting the containers for the first time, the applications need to be setup of course.

After starting, the applications can be found at the following addresses:

 * Jenkins:  http://localhost:8080  
 * Gitlab:  http://localhost  
 * SonarQube:  http://localhost:9000  
 * Portainer:   http://localhost:9090  

The assumption is made the reader knows the applications above, except for Portainer. Portainer is a small application to manage Docker on a host via a web-based GUI.

With the applications configured and running, we can use them to implement the following strategy:
We will use Jenkins as automation server, Gitlab as our source code repository, and SonarQube as our software quality startpoint.
The development street usage can be shown by the image below:

(INSERT IMAGE HERE)

For this, we make use of the Jenkins ‘multibranch pipeline’ feature. In this feature, Jenkins can be configured to periodically poll a source repository (Gitlab in our case) to check if there is any new code or even new branches.  

We also make use of a special ‘cd-pipeline’ source code repository. This repository will hold generic functions that can be used in any other pipeline. With Git tags we can exactly refer which version of the cd-pipeline we would like to use in our ‘regular’ project pipelines.
Regular project repositories should consist of 2 files:  

 * Jenkinsfile  
 * jenkins.properties  

Jenkinsfile is used as main pipeline (programmatically defined what should be done during your development process) which internally refers to a specific cd-pipeline version with the variable ‘TOOLS_VERSION’.  

The latter, jenkins.properties, holds various variables used in the generic functions defined in the ‘cd-pipeline’ repository to be able to do its job correctly. Therefore every regular project pipeline should start with calling the ‘preFlightCheck()’ function.
